import re
file_name = input("Unesite ime datoteke: ")
file = open(file_name)
email_list = []

for line in file:
    if line.startswith("From:"):
        x= re.search(r"[\w\.-]+@[\w\.-]+", line)
        email_list.append(x.group(0))

email_dic = dict()

for k in email_list:
    domain = k.split('@')[1]
    if domain not in email_dic:
        email_dic[domain]= 1
    else:
        email_dic[domain] +=1

print(email_dic)
