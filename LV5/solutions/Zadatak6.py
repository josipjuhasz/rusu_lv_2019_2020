from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as skc
import math
import copy

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X


def getDistance(a, b):
     distance = math.sqrt(pow(a[0]-b[0],2)+pow(a[1]-b[1],2))
     return distance
   

def kmeans(x, n_clusters):
    xCluster = np.random.randint(np.min(x), np.max(x), size = n_clusters)
    yCluster = np.random.randint(np.min(x), np.max(x), size = n_clusters)
    
    y = np.array(list(zip(xCluster, yCluster)), dtype=np.float32)
    firstCluser = np.zeros(y.shape)
    clusters = np.zeros(len(x))
    error = getDistance(y, firstCluser)
    while error != 0:
        for i in range(len(x)):
            distances = getDistance(x[i], y)
            cluster = np.argmin(distances)
            clusters[i] = cluster
            C_old = copy.deepcopy(y)
            for i in range(n_clusters):
                points = [x[j] for j in range(len(x)) if clusters[j] == i]
                y[i] = np.mean(points, axis=0)
                e = getDistance(y, firstCluser)
       
    return clusters

data = generate_data(500,1)
k_means=kmeans(data,3)
plt.scatter(data[:,0],data[:,1],c=k_means)
print(k_means)
