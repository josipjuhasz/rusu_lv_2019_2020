import numpy as np
import matplotlib.pyplot as plt
from sklearn import neural_network as nn
from sklearn import metrics
from sklearn.metrics import mean_squared_error

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1 * varNoise * np.random.normal(0, 1, len(y))
    return y_noisy


def non_func(n):
    x = np.linspace(1, 10, n)
    y = 1.6345 - 0.6235 * np.cos(0.6067 * x) - 1.3501 * np.sin(0.6067 * x) - 1.1622 * np.cos(
        2 * x * 0.6067) - 0.9443 * np.sin(2 * x * 0.6067)
    y_measured = add_noise(y)
    data = np.concatenate((x, y, y_measured), axis=0)
    data = data.reshape(3, n)
    return data.T


data_train = non_func(500)
data_test = non_func(500)
x_train = data_train[:, 0].reshape(-1, 1)
y_train = data_train[:, 1]
y_train_real = data_train[:, 2]
x_test = data_test[:, 0].reshape(-1, 1)
y_test = data_test[:, 1].reshape(-1, 1)


network = nn.MLPRegressor((100, 20), max_iter=20000)
network.fit(x_train, y_train)
predict = network.predict(x_test)
network.fit(x_train, y_train_real)
predict_real = network.predict(x_test)

print(mean_squared_error(y_test, predict))

plt.scatter(x_train, y_train_real, c='silver')
plt.plot(x_test, y_test, label='test', c='orange')
plt.plot(x_test, predict, label='regressor', c='blue')
plt.plot(x_test, predict_real, label='regressor_real', c='teal')
plt.legend()
plt.show()

