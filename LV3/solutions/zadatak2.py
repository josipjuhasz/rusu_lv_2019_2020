import pandas as pd
import matplotlib.pyplot as plt

cars = pd.read_csv('../resources/mtcars.csv')
cars.groupby(cars.cyl).mpg.mean().plot.bar()
plt.xlabel('Cylinder number')
plt.ylabel('Fuel consumption]')
cars.boxplot(column = 'wt', by = ['cyl'])
plt.xlabel('Cylinder number')
plt.ylabel('Weight')
plt.title("")
plt.suptitle("")

cars.boxplot(column = 'mpg', by = ['am']) 
plt.xticks([1, 2], ['Automatic', 'Manual'])
plt.xlabel('Transmission type')
plt.ylabel('Fuel consumption')
plt.title("")
plt.suptitle("")

plt.figure(4)
plt.scatter(cars[cars.am == 0].hp, cars[cars.am == 0].qsec, label = 'Automatic')
plt.scatter(cars[cars.am == 1].hp, cars[cars.am == 1].qsec, label = 'Manual')
plt.xlabel('Horsepower')
plt.ylabel('Acceleration')
plt.legend()
