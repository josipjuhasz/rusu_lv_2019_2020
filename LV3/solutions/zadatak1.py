import pandas as pd

cars = pd.read_csv('../resources/mtcars.csv')


print(.sort_values(['mpg'], ascending = False).head(5)[['Car', 'mpg']])
print(cars[cars.cyl == 8].sort_values(['mpg']).head(3)[['Car', 'mpg', 'cyl']])
print('The average consumption of a 6-cylinder car is %.2f mpg.' %cars[cars.cyl == 6].mpg.mean())
print('The average consumption of a car with 4 cylinders weighing between 2000 and 2200 lbs is %.2f mpg' %cars[(cars.cyl == 4) & (cars.wt >= 2) & (cars.wt <= 2.2)].mpg.mean())
print('%d cars with manual transmission and %d cars with automatic transmission.' %(len(cars[cars.am == 1]), len(cars[cars.am == 0])))
print('%d cars with automatic transmission and power over 100 horsepower.' %len(cars[(cars.am == 0) & (cars.hp > 100)]))

cars['kg'] = cars.wt * 453.592
print(cars[['car', 'kg']])
