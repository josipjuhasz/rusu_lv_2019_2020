import urllib
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt

url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.01.2017&vrijemeDo=31.12.2017.'
airQuality = urllib.request.urlopen(url).read()
root = ET.fromstring(airQuality)

df = pd.DataFrame(columns=('Measurement', 'Time'))

i = 0
while True:
    
    try:
        flag = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['Measurement', 'Time'], [flag[0].text, flag[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.measurement[i] = float(df.measurement[i])
    i = i + 1

df.time = pd.to_datetime(df.time, utc = True)
df.plot(y='Measurement', x='Time');


df['Month'] = df['Time'].dt.month
df['Day of week'] = df['Time'].dt.dayOfweek


print(df.sort_values(['Measurement'], ascending = False).head(3).time.dt.date)

plt.figure(2)
missing = [31,28,31,30,31,30,31,31,30,31,30,31] - df.groupby('Month').measurement.count()
missing.plot.bar()
plt.xlabel('Month')
plt.ylabel('Other')
months = df[(df.month == 2) | (df.month == 7)]
months.boxplot(column = ['Measurement'], by = 'Month')
plt.xticks([1, 2], ['February', 'July'])
plt.xlabel('Month')
plt.ylabel('Concentration')
plt.title("")
plt.suptitle("")
plt.figure(4)
df['Weekend'] = (df.dayOfweek >= 5) & (df.dayOfweek <= 6)
df.boxplot(column = ['Measurement'], by = 'Weekend')
plt.xticks([1, 2], ['Day of week', 'Weekend'])
plt.xlabel('Day')
plt.ylabel('Concentration')
plt.title("")
plt.suptitle("")
