import re

email_names = []

file = open('../resources/mbox-short.txt')

for line in file:
    emails = re.findall('[a-z0-9]+[\._]?[a-z0-9]+[@]\w+', line)
    for email in emails:
        email_names.append(email.split('@')[0])


r = re.compile('\S*a\S*')
prvizadatak = list(filter(r.match, email_names)) 
print(prvizadatak)


r = re.compile('^[^a]*a[^a]*$')
drugizadatak = list(filter(r.match, email_names))
print(drugizadatak)


r = re.compile('^[^a]+$')
trecizadatak = list(filter(r.match, email_names))
print(trecizadatak)


r = re.compile('\S*[0-9]\S*')
cetvrtizadatak = list(filter(r.match, email_names))
print(cetvrtizadatak)


r = re.compile('^[a-z]+$')
petizadatak = list(filter(r.match, email_names))
print(petizadatak)
